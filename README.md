# hacluster role for Red Hat Training class: RH436

### !!! ONLY SUPPORTED IN TRAINING !!!

If you have improvements please create a branch, and a merge request.

## Getting started

To remove your currently installed cluster:

ansible-playbook remove_cluster.yml

To setup a new 4-node cluster with fencing:

ansible-playbook create_cluster.yml

If you want to setup a 2, or 3 node cluster then add then add the -i option with the filename of the inventory representing your configuration. For example:

ansible-playbook -i my_inventory/2 create_cluster.yml

## Important files

remove_cluster.yml - Removes the currently installed cluster from your nodes

create_cluster.yml - Setup a basic 4 node cluster

my_inventory/ - inventory of cluster nodes

